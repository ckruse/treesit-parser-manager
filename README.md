# treesit-parser-manager

treesit-grammar-manager allows you to install, update and remove tree-sitter grammars.

## Requirements

You need a C and C++ compiler.

### macOS

Install the Command Line Tools (CLT) for Xcode (from xcode-select --install or https://developer.apple.com/download/all/) or Xcode 3

### Ubuntu

```
apt install build-essential
```

## Installation from source

Using `straight`

```elisp
(use-package treesit-parser-manager
  :straight (treesit-parser-manager
             :host codeberg
             :repo "ckruse/treesit-parser-manager"
             :files ("*.el"))
  :commands (treesit-parser-manager-install-grammars
             treesit-parser-manager-update-grammars
             treesit-parser-manager-install-or-update-grammars
             treesit-parser-manager-remove-grammar)
  :custom
  (treesit-parser-manager-grammars
   '(("https://github.com/tree-sitter/tree-sitter-rust"
      ("tree-sitter-rust"))

     ("https://github.com/ikatyang/tree-sitter-toml"
      ("tree-sitter-toml"))

     ("https://github.com/tree-sitter/tree-sitter-typescript"
      ("tree-sitter-typescript/tsx" "tree-sitter-typescript/typescript"))

     ("https://github.com/tree-sitter/tree-sitter-javascript"
      ("tree-sitter-javascript"))

     ("https://github.com/tree-sitter/tree-sitter-css"
      ("tree-sitter-css"))

     ("https://github.com/serenadeai/tree-sitter-scss"
      ("tree-sitter-scss"))

     ("https://github.com/tree-sitter/tree-sitter-json"
      ("tree-sitter-json"))))
  :config
  (add-to-list 'treesit-extra-load-path treesit-parser-manager-target-directory)
  :hook (emacs-startup . treesit-parser-manager-install-grammars))
```

Using `package-vc`

```elisp
(use-package treesit-parser-manager
  :vc (:fetcher codeberg :repo "ckruse/treesit-parser-manager")
  :commands (treesit-parser-manager-install-grammars
             treesit-parser-manager-update-grammars
             treesit-parser-manager-install-or-update-grammars
             treesit-parser-manager-remove-grammar)
  :custom
  (treesit-parser-manager-grammars
   '(("https://github.com/tree-sitter/tree-sitter-rust"
      ("tree-sitter-rust"))

     ("https://github.com/ikatyang/tree-sitter-toml"
      ("tree-sitter-toml"))

     ("https://github.com/tree-sitter/tree-sitter-typescript"
      ("tree-sitter-typescript/tsx" "tree-sitter-typescript/typescript"))

     ("https://github.com/tree-sitter/tree-sitter-javascript"
      ("tree-sitter-javascript"))

     ("https://github.com/tree-sitter/tree-sitter-css"
      ("tree-sitter-css"))

     ("https://github.com/serenadeai/tree-sitter-scss"
      ("tree-sitter-scss"))

     ("https://github.com/tree-sitter/tree-sitter-json"
      ("tree-sitter-json"))))
  :config
  (add-to-list 'treesit-extra-load-path 'treesit-parser-manager-target-directory))
  :hook (emacs-startup . treesit-parser-manager-install-grammars))
```

## Installation from Elpa/Melpa

I don’t know shit about how to get it to melpa or elpa.
